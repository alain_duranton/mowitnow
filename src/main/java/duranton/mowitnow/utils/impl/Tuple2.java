/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package duranton.mowitnow.utils.impl;

/**
 *
 * @author disease
 * @param <T>
 * @param <U>
 */
public class Tuple2<T, U> {

    private final T t;
    private final U u;

    public Tuple2(T t, U u) {
        this.t = t;
        this.u = u;
    }

    @Override
    public String toString() {
        return t + " " + u;
    }

}
