/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package duranton.mowitnow.models;

/**
 *
 * @author disease
 */
public enum Order {
    A, D, G;

    public static Order from(String order) {
        switch (order) {
            case "A":
                return A;
            case "D":
                return D;
            case "G":
                return G;
            default:
                throw new IllegalArgumentException("Unknown order");
        }
    }
}
