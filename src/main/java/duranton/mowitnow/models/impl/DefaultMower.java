/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package duranton.mowitnow.models.impl;

import duranton.mowitnow.utils.impl.Completed;
import duranton.mowitnow.utils.Listener;
import duranton.mowitnow.models.Bearing;
import duranton.mowitnow.models.Field;
import duranton.mowitnow.models.Mower;
import duranton.mowitnow.models.Order;
import duranton.mowitnow.models.Point;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author disease
 */
public class DefaultMower implements Mower {

    private Point point;
    private Bearing bearing;
    private Field field;
    private List<Order> orders;
    private final Set<Listener> listeners = new HashSet<>();

    @Override
    public Mower setup(Point point, Bearing bearing, Field field) {
        this.point = point;
        this.bearing = bearing;
        this.field = field;
        return this;
    }

    @Override
    public Mower move(Point point) {
        this.point = point;
        return this;
    }

    @Override
    public Mower attach(Listener listener) {
        listeners.add(listener);
        return this;
    }

    @Override
    public Mower orders(List<Order> orders) {
        this.orders = orders;
        return this;
    }

    @Override
    public Mower executeOrders() {
        for (Order order : orders) {
            if (null != order) {
                switch (order) {
                    case A:
                        field.mayIMoveTo(newPosition(), this);
                        break;
                    case G:
                        bearing = bearing.goPort();
                        break;
                    case D:
                        bearing = bearing.goStarboard();
                        break;
                    default:
                        break;
                }
            }
        }
        for (Listener listener : listeners) {
            listener.notify(new Completed(point, bearing));
        }
        return this;
    }

    private Point newPosition() {
        switch (bearing) {
            case N:
                return point.move(0, 1);
            case S:
                return point.move(0, -1);
            case E:
                return point.move(1, 0);
            case W:
                return point.move(-1, 0);
            default:
                throw new IllegalArgumentException("Unknown bearing");
        }
    }

}
