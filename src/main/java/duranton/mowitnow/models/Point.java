/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package duranton.mowitnow.models;

/**
 *
 * @author disease
 */
public interface Point {

    public Point move(int x, int y);

    public Point isWithin(int i, int i0, int x, int y, Runnable runnable);
    
}
