/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package duranton.mowitnow.models;

import duranton.mowitnow.utils.Listenable;
import java.util.List;

/**
 *
 * @author disease
 */
public interface Mower extends Listenable {

    public Mower setup(Point point, Bearing bearing, Field field);
    
    public Mower move(Point point);
    
    public Mower orders(List<Order> orders);
    
    public Mower executeOrders();
}
