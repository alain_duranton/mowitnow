/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package duranton.mowitnow.models;

/**
 *
 * @author disease
 */
public interface Field {

    public Field mayIMoveTo(Point newPosition, Mower mower);
    
}
