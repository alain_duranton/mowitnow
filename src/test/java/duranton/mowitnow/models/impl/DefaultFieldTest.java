/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package duranton.mowitnow.models.impl;

import duranton.mowitnow.models.Field;
import duranton.mowitnow.models.Mower;
import duranton.mowitnow.models.Point;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author disease
 */
public class DefaultFieldTest {
    
    public DefaultFieldTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of mayIMoveTo method, of class DefaultField.
     */
    @Test
    public void testMayIMoveTo() {
        System.out.println("mayIMoveTo");
        final Point newPosition = mock(Point.class);
        final Mower mower = mock(Mower.class);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                mower.move(newPosition);
            }
        };
        when(newPosition.isWithin(0, 0, 5, 5, runnable)).thenReturn(newPosition);
        DefaultField instance = new DefaultField(5, 5);
        Field expResult = instance;
        Field result = instance.mayIMoveTo(newPosition, mower);
        assertEquals(expResult, result);
        verify(newPosition, atLeastOnce()).isWithin(eq(0), eq(0), eq(5), eq(5), any(Runnable.class));
    }
    
}
