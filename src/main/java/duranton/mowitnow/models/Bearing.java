/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package duranton.mowitnow.models;

/**
 *
 * @author disease
 */
public enum Bearing {

    N(new North()), E(new East()), W(new West()), S(new South());

    private final IBearing bearing;

    private Bearing(IBearing bearing) {
        this.bearing = bearing;
    }

    public static Bearing from(String bearing) {
        switch (bearing) {
            case "N":
                return N;
            case "E":
                return E;
            case "W":
                return W;
            case "S":
                return S;
            default:
                throw new IllegalArgumentException("Unknown bearing");
        }
    }

    public Bearing goPort() {
        return bearing.goPort();
    }

    public Bearing goStarboard() {
        return bearing.goStarboard();
    }

    public static interface IBearing {

        public Bearing goPort();

        public Bearing goStarboard();

    }

    public static class North implements IBearing {

        @Override
        public Bearing goPort() {
            return W;
        }

        @Override
        public Bearing goStarboard() {
            return E;
        }
    }

    public static class East implements IBearing {

        @Override
        public Bearing goPort() {
            return N;
        }

        @Override
        public Bearing goStarboard() {
            return S;
        }
    }

    public static class West implements IBearing {

        @Override
        public Bearing goPort() {
            return S;
        }

        @Override
        public Bearing goStarboard() {
            return N;
        }
    }

    public static class South implements IBearing {

        @Override
        public Bearing goPort() {
            return E;
        }

        @Override
        public Bearing goStarboard() {
            return W;
        }
    }
}
