/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package duranton.mowitnow.models.impl;

import duranton.mowitnow.models.Mower;
import duranton.mowitnow.models.Point;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.Mockito;

/**
 *
 * @author disease
 */
public class DefaultPointTest {
    
    public DefaultPointTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of move method, of class DefaultPoint.
     */
    @Test
    public void testMove() {
        System.out.println("move");
        int x = 0;
        int y = 1;
        DefaultPoint instance = new DefaultPoint(3, 2);
        Point expResult = new DefaultPoint(3, 3);
        Point result = instance.move(x, y);
        assertEquals(expResult, result);
    }

    /**
     * Test of isWithin method, of class DefaultPoint.
     */
    @Test
    public void testIsWithin() {
        System.out.println("isWithin");
        int x0 = 0;
        int y0 = 0;
        int x1 = 5;
        int y1 = 5;
        final DefaultPoint instance = new DefaultPoint(3, 2);
        final Mower mower = Mockito.mock(Mower.class);
        Mockito.when(mower.move(instance)).thenReturn(mower);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                mower.move(instance);
            }
        };
        instance.isWithin(x0, y0, x1, y1, runnable);
        Mockito.verify(mower, Mockito.atLeastOnce()).move(Mockito.eq(instance));
    }
    /**
     * Test of isWithin method, of class DefaultPoint.
     */
    @Test
    public void testIsNotWithin() {
        System.out.println("isNotWithin");
        int x0 = 0;
        int y0 = 0;
        int x1 = 5;
        int y1 = 5;
        final DefaultPoint instance = new DefaultPoint(6, 2);
        final Mower mower = Mockito.mock(Mower.class);
        Mockito.when(mower.move(instance)).thenReturn(mower);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                mower.move(instance);
            }
        };
        instance.isWithin(x0, y0, x1, y1, runnable);
        Mockito.verify(mower, Mockito.never()).move(Mockito.eq(instance));
    }
    
}
