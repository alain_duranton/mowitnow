/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package duranton.mowitnow.view.impl;

import duranton.mowitnow.models.Bearing;
import duranton.mowitnow.models.Point;
import duranton.mowitnow.utils.impl.Tuple2;
import duranton.mowitnow.view.View;
import java.util.List;

/**
 *
 * @author disease
 */
public class StdoutView implements View {

    @Override
    public View render(List<Tuple2<Point, Bearing>> results) {
        for (Tuple2<Point, Bearing> result : results) {
            System.out.println(result);
        }
        return this;
    }
}
