/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package duranton.mowitnow.models.impl;

import duranton.mowitnow.models.Field;
import duranton.mowitnow.models.Mower;
import duranton.mowitnow.models.Point;

/**
 *
 * @author disease
 */
public class DefaultField implements Field {

    private final int x;
    private final int y;

    public DefaultField(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public Field mayIMoveTo(final Point newPosition, final Mower mower) {
        newPosition.isWithin(0, 0, x, y, new Runnable() {
            @Override
            public void run() {
                mower.move(newPosition);
            }
        });
        return this;
    }

}
