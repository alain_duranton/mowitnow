/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package duranton.mowitnow.utils.impl;

import duranton.mowitnow.utils.Event;
import duranton.mowitnow.models.Bearing;
import duranton.mowitnow.models.Point;
import java.util.Objects;

/**
 *
 * @author disease
 */
public class Completed implements Event {

    public final Point point;
    public final Bearing bearing;

    public Completed(Point point, Bearing bearing) {
        this.point = point;
        this.bearing = bearing;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 13 * hash + Objects.hashCode(this.point);
        hash = 13 * hash + Objects.hashCode(this.bearing);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Completed other = (Completed) obj;
        if (!Objects.equals(this.point, other.point)) {
            return false;
        }
        return this.bearing == other.bearing;
    }

}
