/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package duranton.mowitnow.models.impl;

import duranton.mowitnow.models.Point;

/**
 *
 * @author disease
 */
public class DefaultPoint implements Point {

    private final int x;
    private final int y;

    public DefaultPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public Point move(int x, int y) {
        return new DefaultPoint(this.x + x, this.y + y);
    }

    @Override
    public Point isWithin(int x0, int y0, int x1, int y1, Runnable runnable) {
        if (x >= x0 && x <= x1 && y >= y0 && y <= y1) {
            runnable.run();
        }
        return this;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + this.x;
        hash = 31 * hash + this.y;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DefaultPoint other = (DefaultPoint) obj;
        if (this.x != other.x) {
            return false;
        }
        return this.y == other.y;
    }

    @Override
    public String toString() {
        return x + " " + y;
    }

    
}
