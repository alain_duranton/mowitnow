/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package duranton.mowitnow.models.impl;

import duranton.mowitnow.utils.impl.Completed;
import duranton.mowitnow.utils.Event;
import duranton.mowitnow.utils.Listener;
import static duranton.mowitnow.models.Bearing.E;
import static duranton.mowitnow.models.Bearing.N;
import static duranton.mowitnow.models.Bearing.W;
import duranton.mowitnow.models.Order;
import static duranton.mowitnow.models.Order.A;
import static duranton.mowitnow.models.Order.D;
import static duranton.mowitnow.models.Order.G;
import java.util.ArrayList;
import static java.util.Arrays.asList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author disease
 */
public class DefaultMowerTest {

    public DefaultMowerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void noOrders() {
        List<Order> orders = new ArrayList<>();
        Listener listener = mock(Listener.class);
        Event event = new Completed(new DefaultPoint(1, 2), N);
        when(listener.notify(event)).thenReturn(listener);
        DefaultMower instance = new DefaultMower();
        instance.attach(listener).setup(new DefaultPoint(1, 2), N, new DefaultField(5, 5)).orders(orders).executeOrders();
        verify(listener, atLeastOnce()).notify(eq(event));
    }
    
    @Test
    public void run1() {
        List<Order> orders = asList(new Order[]{G,A,G,A,G,A,G,A,A});
        Listener listener = mock(Listener.class);
        Event event = new Completed(new DefaultPoint(1, 3), N);
        when(listener.notify(event)).thenReturn(listener);
        DefaultMower instance = new DefaultMower();
        instance.attach(listener).setup(new DefaultPoint(1, 2), N, new DefaultField(5, 5)).orders(orders).executeOrders();
        verify(listener, atLeastOnce()).notify(eq(event));
    }

    @Test
    public void run2() {
        List<Order> orders = asList(new Order[]{A,A,D,A,A,D,A,D,D,A});
        Listener listener = mock(Listener.class);
        Event event = new Completed(new DefaultPoint(5, 1), E);
        when(listener.notify(event)).thenReturn(listener);
        DefaultMower instance = new DefaultMower();
        instance.attach(listener).setup(new DefaultPoint(3, 3), E, new DefaultField(5, 5)).orders(orders).executeOrders();
        verify(listener, atLeastOnce()).notify(eq(event));
    }

}
