/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package duranton.mowitnow;

import duranton.mowitnow.controller.MowersController;
import duranton.mowitnow.models.Bearing;
import duranton.mowitnow.models.Field;
import duranton.mowitnow.models.Mower;
import duranton.mowitnow.models.Order;
import duranton.mowitnow.models.Point;
import duranton.mowitnow.models.impl.DefaultField;
import duranton.mowitnow.models.impl.DefaultMower;
import duranton.mowitnow.models.impl.DefaultPoint;
import duranton.mowitnow.utils.impl.Completed;
import duranton.mowitnow.utils.Event;
import duranton.mowitnow.utils.Listenable;
import duranton.mowitnow.utils.Listener;
import duranton.mowitnow.utils.impl.Ended;
import duranton.mowitnow.utils.impl.Tuple2;
import duranton.mowitnow.view.View;
import duranton.mowitnow.view.impl.StdoutView;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import static java.lang.Integer.valueOf;
import org.apache.commons.cli.Option;

/**
 *
 * @author disease
 */
public class App  {


    /**
     * @param args the command line arguments
     * @throws org.apache.commons.cli.ParseException
     */
    public static void main(String[] args) throws ParseException {
        CommandLineParser parser = new DefaultParser();
        Options options = new Options();
        Option option = new Option("f", "file", true, "input file");
        option.setRequired(true);
        options.addOption(option);
        CommandLine parse = parser.parse(options, args);
        String filename = parse.getOptionValue("f");
        File f = new File(filename);
        MowersController controller = new MowersController(new StdoutView());
        controller.runFile(f);
    }

}
