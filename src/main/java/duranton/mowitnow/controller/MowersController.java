/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package duranton.mowitnow.controller;

import duranton.mowitnow.models.Bearing;
import duranton.mowitnow.models.Field;
import duranton.mowitnow.models.Mower;
import duranton.mowitnow.models.Order;
import duranton.mowitnow.models.Point;
import duranton.mowitnow.models.impl.DefaultField;
import duranton.mowitnow.models.impl.DefaultMower;
import duranton.mowitnow.models.impl.DefaultPoint;
import duranton.mowitnow.utils.Event;
import duranton.mowitnow.utils.Listenable;
import duranton.mowitnow.utils.Listener;
import duranton.mowitnow.utils.impl.Completed;
import duranton.mowitnow.utils.impl.Ended;
import duranton.mowitnow.utils.impl.Tuple2;
import duranton.mowitnow.view.View;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import static java.lang.Integer.valueOf;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author disease
 */
public class MowersController implements Listener, Listenable {

    private final Pattern fieldPattern = Pattern.compile("^(\\d+) (\\d+)$");
    private final Pattern mowerInitPattern = Pattern.compile("^(\\d+) (\\d+) ([NEWS])$");
    private final List<Mower> mowers = new ArrayList<>();
    private Field field;
    private final List<Tuple2<Point, Bearing>> results = new ArrayList<>();
    private final View view;
    private final Set<Listener> listeners = new HashSet<>();

    public MowersController(View view) {
        this.view = view;
    }

    @Override
    public Listener notify(Event event) {
        if (event instanceof Completed) {
            results.add(new Tuple2(((Completed) event).point, ((Completed) event).bearing));
        }
        if (event instanceof Ended) {
            view.render(results);
        }
        return this;
    }

    @Override
    public Listenable attach(Listener listener) {
        listeners.add(listener);
        return this;
    }

    private MowersController getField(BufferedReader r) throws IOException {
        String fieldSetup = r.readLine();
        Matcher m = fieldPattern.matcher(fieldSetup);
        m.matches();
        field = new DefaultField(valueOf(m.group(1)), valueOf(m.group(2)));
        return this;
    }

    private MowersController thenAddMower(String mowerSetup, String mowerOrders) {
        Matcher m = mowerInitPattern.matcher(mowerSetup);
        m.matches();
        List<Order> orders = new ArrayList<>(mowerOrders.length());
        for (Character character : mowerOrders.toCharArray()) {
            orders.add(Order.from(String.valueOf(character)));
        }
        Mower mower = (Mower) new DefaultMower()
            .setup(new DefaultPoint(valueOf(m.group(1)), valueOf(m.group(2))), Bearing.from(m.group(3)), field)
            .orders(orders)
            .attach(this);
        mowers.add(mower);
        return this;
    }

    private MowersController thenAddAllMower(BufferedReader r) throws IOException {
        String mowerSetup;
        String mowerOrders;
        while (null != (mowerSetup = r.readLine()) && null != (mowerOrders = r.readLine())) {
            thenAddMower(mowerSetup, mowerOrders);
        }
        return this;
    }

    public MowersController runFile(File file) {
        try (BufferedReader r = new BufferedReader(new FileReader(file))) {
            getField(r).thenAddAllMower(r).attach(this);
            for (Mower mower : mowers) {
                mower.executeOrders();
            }
            for (Listener listener : listeners) {
                listener.notify(new Ended());
            }
        } catch (IOException ex) {
            Logger.getLogger(MowersController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return this;
    }
}
